package ru.kolevatykh.tm;

import org.apache.log4j.PropertyConfigurator;
import org.jetbrains.annotations.NotNull;
import ru.kolevatykh.tm.bootstrap.Bootstrap;

public final class Application {
    public static void main(String[] args) {
        String log4jConfPath = "./src/main/resources/META-INF/log4j.properties";
        PropertyConfigurator.configure(log4jConfPath);
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.init();
    }
}
