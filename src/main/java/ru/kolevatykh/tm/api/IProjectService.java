package ru.kolevatykh.tm.api;

import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.entity.Project;

import java.util.List;

public interface IProjectService extends IProjectTaskService<Project> {
    @Nullable List<Project> findAll(@Nullable String userId);

    @Nullable Project findOneById(@Nullable String userId, @Nullable String id);

    @Nullable Project findOneByName(@Nullable String userId, @Nullable String name);

    void persist(@Nullable Project project);

    void merge(@Nullable Project project);

    void remove(@Nullable String userId, @Nullable String id);

    void removeAll(@Nullable String userId);

    @Nullable List<Project> findAllSortedByStartDate(@Nullable final String userId);

    @Nullable List<Project> findAllSortedByEndDate(@Nullable final String userId);

    @Nullable List<Project> findAllSortedByStatus(@Nullable final String userId);
}