package ru.kolevatykh.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IRepository<T> {
    @NotNull List<T> findAll();

    @Nullable T findOneById(@NotNull String id);

    void persist(@NotNull T entity);

    void merge(@NotNull T entity);

    void remove(@NotNull String id);

    void removeAll();
}
