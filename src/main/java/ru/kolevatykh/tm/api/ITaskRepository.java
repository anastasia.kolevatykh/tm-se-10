package ru.kolevatykh.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.entity.Task;

import java.util.List;

public interface ITaskRepository extends IProjectTaskRepository<Task> {
    @NotNull List<Task> findAll(@NotNull String userId);

    @Nullable Task findOneById(@NotNull String userId, @NotNull String id);

    @Nullable Task findOneByName(@NotNull String userId, @NotNull String name);

    void persist(@NotNull Task task);

    void merge(@NotNull Task task);

    void remove(@NotNull String userId, @NotNull String id);

    void removeAll(@NotNull String userId);

    void removeTasksWithProjectId(@NotNull String userId);

    void removeProjectTasks(@NotNull String userId, @NotNull String projectId);

    @NotNull List<Task> findTasksByProjectId(@NotNull String userId, @NotNull String projectId);

    @NotNull List<Task> findTasksWithoutProject(@NotNull String userId);

    void assignToProject(@NotNull String userId, @NotNull String id, @NotNull String projectId);

    @NotNull List<Task> findAllSortedByStartDate(@NotNull final String userId);

    @NotNull List<Task> findAllSortedByEndDate(@NotNull final String userId);

    @NotNull List<Task> findAllSortedByStatus(@NotNull final String userId);
}
