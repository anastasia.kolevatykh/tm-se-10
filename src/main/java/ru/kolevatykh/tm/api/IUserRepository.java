package ru.kolevatykh.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.entity.User;

import java.util.List;

public interface IUserRepository extends IRepository<User> {
    @NotNull List<User> findAll();

    @Nullable User findOneById(@NotNull String id);

    @Nullable User findOneByLogin(@NotNull String login);

    void persist(@NotNull User user);

    void merge(@NotNull User user);

    void remove(@NotNull String id);

    void removeAll();
}
