package ru.kolevatykh.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.entity.User;

import java.util.List;

public interface IUserService extends IService<User> {
    @Nullable User getCurrentUser();

    void setCurrentUser(@Nullable User user);

    @NotNull List<User> findAll();

    @Nullable User findOneById(@Nullable String id);

    @Nullable User findOneByLogin(@Nullable String login);

    void persist(@Nullable User user);

    void merge(@Nullable User user);

    void remove(@Nullable String id);

    void removeAll();

    void createTestUsers();
}
