package ru.kolevatykh.tm.command.data;

import org.jetbrains.annotations.NotNull;
import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.entity.Domain;
import ru.kolevatykh.tm.entity.Project;
import ru.kolevatykh.tm.entity.Task;
import ru.kolevatykh.tm.entity.User;
import ru.kolevatykh.tm.enumerate.RoleType;

import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.List;

public final class DeserializationLoadCommand extends AbstractCommand {
    @NotNull
    protected final List<RoleType> roleTypes = new ArrayList<>();

    @NotNull
    @Override
    public String getName() {
        return "load";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "l";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "\tLoad data from file.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @NotNull
    @Override
    public List<RoleType> isRoleAllowed() {
        roleTypes.add(RoleType.USER);
        roleTypes.add(RoleType.ADMIN);
        return roleTypes;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[LOAD (deserialization)]");

        @NotNull final ObjectInputStream objectInputStream =
                new ObjectInputStream(new FileInputStream("./src/main/resources/file/domain.out"));
        @NotNull final Domain domain = (Domain) objectInputStream.readObject();
        objectInputStream.close();

        if (domain.getUsers() != null) {
            for (@NotNull final User user : domain.getUsers())
                serviceLocator.getUserService().persist(user);
        }
        if (domain.getProjects() != null) {
            for (@NotNull final Project project : domain.getProjects())
                serviceLocator.getProjectService().persist(project);
        }
        if (domain.getTasks() != null) {
            for (@NotNull final Task task : domain.getTasks())
                serviceLocator.getTaskService().persist(task);
        }

        System.out.println("[Loading is successful!]");
    }
}
