package ru.kolevatykh.tm.command.data;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.jetbrains.annotations.NotNull;
import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.entity.Domain;
import ru.kolevatykh.tm.entity.Project;
import ru.kolevatykh.tm.entity.Task;
import ru.kolevatykh.tm.entity.User;
import ru.kolevatykh.tm.enumerate.RoleType;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public final class JacksonLoadXmlCommand extends AbstractCommand {
    @NotNull
    protected final List<RoleType> roleTypes = new ArrayList<>();

    @NotNull
    @Override
    public String getName() {
        return "jackson-xml-Load";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "xl";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "\tSave to xml file Jackson.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @NotNull
    @Override
    public List<RoleType> isRoleAllowed() {
        roleTypes.add(RoleType.USER);
        roleTypes.add(RoleType.ADMIN);
        return roleTypes;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[JACKSON XML LOAD]");

        @NotNull final XmlMapper xmlMapper = new XmlMapper();
        @NotNull final Domain domain = xmlMapper
                .readValue(new File("./src/main/resources/file/jacksonDomain.xml"), Domain.class);

        if (domain.getUsers() != null) {
            for (@NotNull final User user : domain.getUsers())
                serviceLocator.getUserService().persist(user);
        }
        if (domain.getProjects() != null) {
            for (@NotNull final Project project : domain.getProjects())
                serviceLocator.getProjectService().persist(project);
        }
        if (domain.getTasks() != null) {
            for (@NotNull final Task task : domain.getTasks())
                serviceLocator.getTaskService().persist(task);
        }

        System.out.println("[Saving is successful!]");
    }
}
