package ru.kolevatykh.tm.command.data;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.entity.Domain;
import ru.kolevatykh.tm.entity.User;
import ru.kolevatykh.tm.enumerate.RoleType;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public final class JacksonSaveXmlCommand extends AbstractCommand {
    @NotNull
    protected final List<RoleType> roleTypes = new ArrayList<>();

    @NotNull
    @Override
    public String getName() {
        return "jackson-xml-save";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "xs";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "\tSave to xml file Jackson.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @NotNull
    @Override
    public List<RoleType> isRoleAllowed() {
        roleTypes.add(RoleType.USER);
        roleTypes.add(RoleType.ADMIN);
        return roleTypes;
    }

    @Override
    public void execute() throws Exception {
        @Nullable final User user = serviceLocator.getUserService().getCurrentUser();
        if (user == null) return;
        @NotNull final String userId = user.getId();

        System.out.println("[JACKSON XML SAVE]");

        @NotNull final Domain domain = new Domain();
        domain.setUsers(serviceLocator.getUserService().findAll());
        domain.setProjects(serviceLocator.getProjectService().findAll(userId));
        domain.setTasks(serviceLocator.getTaskService().findAll(userId));

        @NotNull final XmlMapper xmlMapper = new XmlMapper();
        xmlMapper
                .writerWithDefaultPrettyPrinter()
                .writeValue(new File("./src/main/resources/file/jacksonDomain.xml"), domain);

        System.out.println("[Saving is successful!]");
    }
}
