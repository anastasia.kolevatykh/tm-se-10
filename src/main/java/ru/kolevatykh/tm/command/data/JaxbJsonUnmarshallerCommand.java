package ru.kolevatykh.tm.command.data;

import org.eclipse.persistence.jaxb.JAXBContextFactory;
import org.eclipse.persistence.jaxb.UnmarshallerProperties;
import org.jetbrains.annotations.NotNull;
import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.entity.Domain;
import ru.kolevatykh.tm.entity.Project;
import ru.kolevatykh.tm.entity.Task;
import ru.kolevatykh.tm.entity.User;
import ru.kolevatykh.tm.enumerate.RoleType;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class JaxbJsonUnmarshallerCommand extends AbstractCommand {
    @NotNull
    protected final List<RoleType> roleTypes = new ArrayList<>();

    @NotNull
    @Override
    public String getName() {
        return "json-unmarshaller";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "ju";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "\tLoad from json file JAX-B.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @NotNull
    @Override
    public List<RoleType> isRoleAllowed() {
        roleTypes.add(RoleType.USER);
        roleTypes.add(RoleType.ADMIN);
        return roleTypes;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[JSON UNMARSHALLER]");

        System.setProperty("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
        @NotNull final Map<String, Object> properties = new HashMap<>();
        properties.put("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
        @NotNull final JAXBContext context = JAXBContextFactory.createContext(new Class[] {Domain.class}, properties);
        @NotNull final Unmarshaller unmarshaller = context.createUnmarshaller();
        unmarshaller.setProperty(UnmarshallerProperties.MEDIA_TYPE, "application/json");
        unmarshaller.setProperty("eclipselink.json.include-root", false);

        @NotNull final StreamSource source =
                new StreamSource(new File("./src/main/resources/file/jaxbDomain.json"));
        @NotNull final JAXBElement<Domain> jaxbElement = unmarshaller.unmarshal(source, Domain.class);
        @NotNull final Domain domain = jaxbElement.getValue();

        if (domain.getUsers() != null) {
            for (@NotNull final User user : domain.getUsers())
                serviceLocator.getUserService().persist(user);
        }
        if (domain.getProjects() != null) {
            for (@NotNull final Project project : domain.getProjects())
                serviceLocator.getProjectService().persist(project);
        }
        if (domain.getTasks() != null) {
            for (@NotNull final Task task : domain.getTasks())
                serviceLocator.getTaskService().persist(task);
        }

        System.out.println("[Loading is successful!]");
    }
}
