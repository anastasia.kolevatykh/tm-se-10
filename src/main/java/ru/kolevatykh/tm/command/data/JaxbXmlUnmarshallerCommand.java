package ru.kolevatykh.tm.command.data;

import org.eclipse.persistence.jaxb.JAXBContextFactory;
import org.jetbrains.annotations.NotNull;
import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.entity.Domain;
import ru.kolevatykh.tm.entity.Project;
import ru.kolevatykh.tm.entity.Task;
import ru.kolevatykh.tm.entity.User;
import ru.kolevatykh.tm.enumerate.RoleType;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class JaxbXmlUnmarshallerCommand extends AbstractCommand {
    @NotNull
    protected final List<RoleType> roleTypes = new ArrayList<>();

    @NotNull
    @Override
    public String getName() {
        return "xml-unmarshaller";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "xu";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "\tLoad from xml file JAX-B.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @NotNull
    @Override
    public List<RoleType> isRoleAllowed() {
        roleTypes.add(RoleType.USER);
        roleTypes.add(RoleType.ADMIN);
        return roleTypes;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[XML UNMARSHALLER]");

        System.setProperty("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
        @NotNull final Map<String, Object> properties = new HashMap<>();
        properties.put("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
        @NotNull final JAXBContext context = JAXBContextFactory.createContext(new Class[] {Domain.class}, properties);
        @NotNull final Unmarshaller unmarshaller = context.createUnmarshaller();
        @NotNull final Domain domain =
                (Domain) unmarshaller.unmarshal(new File("./src/main/resources/file/jaxbDomain.xml"));

        if (domain.getUsers() != null) {
            for (@NotNull final User user : domain.getUsers())
                serviceLocator.getUserService().persist(user);
        }
        if (domain.getProjects() != null) {
            for (@NotNull final Project project : domain.getProjects())
                serviceLocator.getProjectService().persist(project);
        }
        if (domain.getTasks() != null) {
            for (@NotNull final Task task : domain.getTasks())
                serviceLocator.getTaskService().persist(task);
        }

        System.out.println("[Loading is successful!]");
    }
}
