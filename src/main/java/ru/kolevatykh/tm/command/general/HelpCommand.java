package ru.kolevatykh.tm.command.general;

import org.jetbrains.annotations.NotNull;
import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.enumerate.RoleType;

import java.util.ArrayList;
import java.util.List;

public final class HelpCommand extends AbstractCommand {
    @NotNull
    protected final List<RoleType> roleTypes = new ArrayList<>();

    @NotNull
    @Override
    public String getName() {
        return "help";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "h";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "\t\tShow all commands.";
    }

    @Override
    public boolean needAuth() {
        return false;
    }

    @NotNull
    @Override
    public List<RoleType> isRoleAllowed() {
        roleTypes.add(RoleType.USER);
        roleTypes.add(RoleType.ADMIN);
        return roleTypes;
    }

    @Override
    public void execute() {
        for (@NotNull final AbstractCommand command : serviceLocator.getCommands()) {
            System.out.println(command.getName() + ": " + command.getDescription());
        }
    }
}
