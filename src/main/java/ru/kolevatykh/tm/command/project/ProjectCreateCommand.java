package ru.kolevatykh.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.entity.Project;
import ru.kolevatykh.tm.entity.User;
import ru.kolevatykh.tm.enumerate.RoleType;
import ru.kolevatykh.tm.util.ConsoleInputUtil;
import ru.kolevatykh.tm.util.DateFormatterUtil;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public final class ProjectCreateCommand extends AbstractCommand {

    @NotNull
    protected final List<RoleType> roleTypes = new ArrayList<>();

    @NotNull
    @Override
    public String getName() {
        return "project-create";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "pcr";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Create new project.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @NotNull
    @Override
    public List<RoleType> isRoleAllowed() {
        roleTypes.add(RoleType.USER);
        roleTypes.add(RoleType.ADMIN);
        return roleTypes;
    }

    @Override
    public void execute() throws Exception {
        @Nullable final User user = serviceLocator.getUserService().getCurrentUser();
        if (user == null) return;
        @NotNull final String userId = user.getId();

        System.out.println("[PROJECT CREATE]\nEnter project name: ");
        @NotNull final String name = ConsoleInputUtil.getConsoleInput();

        if (name.isEmpty()) {
            throw new Exception("[The name can't be empty.]");
        }

        System.out.println("Enter project description: ");
        @NotNull final String description = ConsoleInputUtil.getConsoleInput();

        System.out.println("Enter project start date: ");
        @NotNull final String projectStartDate = ConsoleInputUtil.getConsoleInput();
        @Nullable final Date startDate = DateFormatterUtil.parseDate(projectStartDate);

        System.out.println("Enter project end date: ");
        @NotNull final String projectEndDate = ConsoleInputUtil.getConsoleInput();
        @Nullable final Date endDate = DateFormatterUtil.parseDate(projectEndDate);

        @NotNull final Project project = new Project(name, description, startDate, endDate);
        project.setUserId(userId);

        serviceLocator.getProjectService().persist(project);
        System.out.println("[OK]");
    }
}
