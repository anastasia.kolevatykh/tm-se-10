package ru.kolevatykh.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.entity.Project;
import ru.kolevatykh.tm.entity.User;
import ru.kolevatykh.tm.enumerate.RoleType;
import ru.kolevatykh.tm.util.ConsoleInputUtil;
import ru.kolevatykh.tm.util.DateFormatterUtil;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public final class ProjectUpdateCommand extends AbstractCommand {
    @NotNull
    protected final List<RoleType> roleTypes = new ArrayList<>();

    @NotNull
    @Override
    public String getName() {
        return "project-update";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "pu";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Update selected project.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @NotNull
    @Override
    public List<RoleType> isRoleAllowed() {
        roleTypes.add(RoleType.USER);
        roleTypes.add(RoleType.ADMIN);
        return roleTypes;
    }

    @Override
    public void execute() throws Exception {
        @Nullable final User user = serviceLocator.getUserService().getCurrentUser();
        if (user == null) return;
        @NotNull final String userId = user.getId();

        System.out.println("[PROJECT UPDATE]\nEnter project id: ");
        @NotNull final String id = ConsoleInputUtil.getConsoleInput();

        if (id.isEmpty()) {
            throw new Exception("[The name can't be empty.]");
        }

        @Nullable final Project project = serviceLocator.getProjectService().findOneById(userId, id);

        if (project == null) {
            throw new Exception("[The project '" + id + "' does not exist!]");
        }

        System.out.println("Enter new project name: ");
        @NotNull final String nameNew = ConsoleInputUtil.getConsoleInput();

        System.out.println("Enter new project description: ");
        @NotNull final String descriptionNew = ConsoleInputUtil.getConsoleInput();

        System.out.println("Enter project start date: ");
        @NotNull final String startNew = ConsoleInputUtil.getConsoleInput();
        @Nullable final Date startDateNew = DateFormatterUtil.parseDate(startNew);

        System.out.println("Enter project end date: ");
        @NotNull final String endNew = ConsoleInputUtil.getConsoleInput();
        @Nullable final Date endDateNew = DateFormatterUtil.parseDate(endNew);

        project.setName(nameNew);
        project.setDescription(descriptionNew);
        project.setStartDate(startDateNew);
        project.setEndDate(endDateNew);

        serviceLocator.getProjectService().merge(project);
        System.out.println("[OK]");
    }
}
