package ru.kolevatykh.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.entity.Task;
import ru.kolevatykh.tm.entity.User;
import ru.kolevatykh.tm.enumerate.RoleType;
import ru.kolevatykh.tm.util.ConsoleInputUtil;
import ru.kolevatykh.tm.util.DateFormatterUtil;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public final class TaskUpdateCommand extends AbstractCommand {
    @NotNull
    protected final List<RoleType> roleTypes = new ArrayList<>();

    @NotNull
    @Override
    public String getName() {
        return "task-update";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "tu";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "\tUpdate selected task.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @NotNull
    @Override
    public List<RoleType> isRoleAllowed() {
        roleTypes.add(RoleType.USER);
        roleTypes.add(RoleType.ADMIN);
        return roleTypes;
    }

    @Override
    public void execute() throws Exception {
        @Nullable final User user = serviceLocator.getUserService().getCurrentUser();
        if (user == null) return;
        @NotNull final String userId = user.getId();

        System.out.println("[TASK UPDATE]\nEnter task id: ");
        @NotNull final String id = ConsoleInputUtil.getConsoleInput();

        if (id.isEmpty()) {
            throw new Exception("[The id can't be empty.]");
        }

        @Nullable final Task task = serviceLocator.getTaskService().findOneById(userId, id);

        if (task == null) {
            throw new Exception("[The task '" + id + "' does not exist!]");
        }

        System.out.println("Enter new task name: ");
        @NotNull final String nameNew = ConsoleInputUtil.getConsoleInput();

        if (nameNew.isEmpty()) {
            throw new Exception("[The name can't be empty.]");
        }

        System.out.println("Enter new task description: ");
        @NotNull final String descriptionNew = ConsoleInputUtil.getConsoleInput();

        System.out.println("Enter new task start date: ");
        @NotNull final String startNew = ConsoleInputUtil.getConsoleInput();
        @Nullable final Date startDateNew = DateFormatterUtil.parseDate(startNew);

        System.out.println("Enter new task end date: ");
        @NotNull final String endNew = ConsoleInputUtil.getConsoleInput();
        @Nullable final Date endDateNew = DateFormatterUtil.parseDate(endNew);

        task.setName(nameNew);
        task.setDescription(descriptionNew);
        task.setStartDate(startDateNew);
        task.setEndDate(endDateNew);

        serviceLocator.getTaskService().merge(task);
        System.out.println("[OK]");
    }
}
