package ru.kolevatykh.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.entity.Task;
import ru.kolevatykh.tm.entity.User;
import ru.kolevatykh.tm.enumerate.RoleType;
import ru.kolevatykh.tm.enumerate.StatusType;
import ru.kolevatykh.tm.util.ConsoleInputUtil;
import ru.kolevatykh.tm.util.DateFormatterUtil;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public final class TaskUpdateStatusCommand extends AbstractCommand {
    @NotNull
    protected final List<RoleType> roleTypes = new ArrayList<>();

    @NotNull
    @Override
    public String getName() {
        return "task-update-status";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "tus";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "\tUpdate selected task status.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @NotNull
    @Override
    public List<RoleType> isRoleAllowed() {
        roleTypes.add(RoleType.USER);
        roleTypes.add(RoleType.ADMIN);
        return roleTypes;
    }

    @Override
    public void execute() throws Exception {
        @Nullable final User user = serviceLocator.getUserService().getCurrentUser();
        if (user == null) return;
        @NotNull final String userId = user.getId();

        System.out.println("[TASK UPDATE STATUS]\nEnter task id: ");
        @NotNull final String id = ConsoleInputUtil.getConsoleInput();

        if (id.isEmpty()) {
            throw new Exception("[The name can't be empty.]");
        }

        @Nullable final Task task = serviceLocator.getTaskService().findOneById(userId, id);

        if (task == null) {
            throw new Exception("[The task '" + id + "' does not exist!]");
        }

        System.out.println("Enter new STATUS name: PLANNED INPROCESS READY");
        @NotNull final String statusNew = ConsoleInputUtil.getConsoleInput();

        if (statusNew.isEmpty()) {
            throw new Exception("[The status can't be empty.]");
        }

        switch(StatusType.valueOf(statusNew)) {
            case PLANNED:
                task.setStatusType(StatusType.PLANNED);
                break;
            case INPROCESS:
                task.setStatusType(StatusType.INPROCESS);
                break;
            case READY:
                task.setStatusType(StatusType.READY);
                break;
            default:
                System.out.println("[The status doesn't exist.]");
                return;
        }

        serviceLocator.getTaskService().merge(task);
        System.out.println("[OK]");
    }
}
