package ru.kolevatykh.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.entity.User;
import ru.kolevatykh.tm.enumerate.RoleType;
import ru.kolevatykh.tm.util.ConsoleInputUtil;

import java.util.ArrayList;
import java.util.List;

public final class UserLogOutCommand extends AbstractCommand {
    @NotNull
    protected final List<RoleType> roleTypes = new ArrayList<>();

    @NotNull
    @Override
    public String getName() {
        return "user-logout";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "ulo";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "\tLogout from account.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @NotNull
    @Override
    public List<RoleType> isRoleAllowed() {
        roleTypes.add(RoleType.USER);
        roleTypes.add(RoleType.ADMIN);
        return roleTypes;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[USER LOGOUT]\nEnter login to confirm logout: ");
        @NotNull final String login = ConsoleInputUtil.getConsoleInput();

        if (login.isEmpty()) {
            throw new Exception("[The name can't be empty.]");
        }

        @Nullable final User currentUser = serviceLocator.getUserService().getCurrentUser();
        @Nullable final User user = serviceLocator.getUserService().findOneByLogin(login);

        if (user == null || currentUser == null) {
            throw new Exception("[The login '" + login + "' does not exist!]");
        }

        if (!currentUser.getLogin().equals(user.getLogin())) {
            throw new Exception("[The login '" + login + "' is wrong! Enter correct login.]");
        }

        System.out.println("Confirm logout, y/n: ");
        @NotNull final String answer = ConsoleInputUtil.getConsoleInput();

        if (answer.equals("y")) {
            user.setAuth(false);
            serviceLocator.getUserService().setCurrentUser(null);
            System.out.println("[OK]");
        }
    }
}
