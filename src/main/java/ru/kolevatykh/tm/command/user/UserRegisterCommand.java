package ru.kolevatykh.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.enumerate.RoleType;
import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.entity.User;
import ru.kolevatykh.tm.util.ConsoleInputUtil;

import java.util.ArrayList;
import java.util.List;

public final class UserRegisterCommand extends AbstractCommand {
    @NotNull
    protected final List<RoleType> roleTypes = new ArrayList<>();

    @NotNull
    @Override
    public String getName() {
        return "user-register";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "ur";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "\tRegister new user.";
    }

    @Override
    public boolean needAuth() {
        return false;
    }

    @NotNull
    @Override
    public List<RoleType> isRoleAllowed() {
        roleTypes.add(RoleType.USER);
        roleTypes.add(RoleType.ADMIN);
        return roleTypes;
    }

    @Override
    public void execute() throws Exception {
        @Nullable final User currentUser = serviceLocator.getUserService().getCurrentUser();

        if (currentUser != null) {
            throw new Exception("[You are authorized under login: " + currentUser.getLogin()
                    + "\nPlease LOGOUT first, in order to register new account.]");
        }

        System.out.println("[USER REGISTRATION]\nEnter your login: ");
        @NotNull final String login = ConsoleInputUtil.getConsoleInput();

        if (login.isEmpty()) {
            throw new Exception("[The name can't be empty.]");
        }

        @Nullable final User user = serviceLocator.getUserService().findOneByLogin(login);

        if (user != null) {
            throw new Exception("[Login '" + login + "' already exists. Please, retry.]");
        }

        System.out.println("Enter your password: ");
        @NotNull final String password = ConsoleInputUtil.getConsoleInput();

        if (password.isEmpty()) {
            throw new Exception("[The password can't be empty.]");
        }

        @NotNull final User userNew = new User();
        userNew.setLogin(login);
        userNew.setPasswordHash(password);
        userNew.setRoleType(RoleType.USER);
        userNew.setAuth(true);

        serviceLocator.getUserService().persist(userNew);
        serviceLocator.getUserService().setCurrentUser(userNew);
        System.out.println("[OK]");
    }
}
