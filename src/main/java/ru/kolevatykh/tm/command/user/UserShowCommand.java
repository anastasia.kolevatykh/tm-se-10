package ru.kolevatykh.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.entity.Project;
import ru.kolevatykh.tm.entity.Task;
import ru.kolevatykh.tm.entity.User;
import ru.kolevatykh.tm.enumerate.RoleType;

import java.util.ArrayList;
import java.util.List;

public final class UserShowCommand extends AbstractCommand {
    @NotNull
    protected final List<RoleType> roleTypes = new ArrayList<>();

    @NotNull
    @Override
    public String getName() {
        return "user-show";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "ush";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "\tShow all projects and tasks.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @NotNull
    @Override
    public List<RoleType> isRoleAllowed() {
        roleTypes.add(RoleType.USER);
        roleTypes.add(RoleType.ADMIN);
        return roleTypes;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[USER SHOW]");

        @Nullable final User user = serviceLocator.getUserService().getCurrentUser();

        if (user == null) {
            throw new Exception("[The user does not exist!]");
        }

        System.out.println("[LOGIN]");
        System.out.println(user.getLogin());
        System.out.println("[ROLE]");
        System.out.println(user.getRoleType().displayName());

        @Nullable final List<Project> projectList = serviceLocator.getProjectService().findAll(user.getId());

        @Nullable final List<Task> taskNotAssignedList = serviceLocator.getTaskService()
                .findTasksWithoutProject(user.getId());

        if (projectList != null) {
            System.out.println("[PROJECT LIST]");
            int projectCount = 0;

            for (@NotNull final Project project : projectList) {
                @Nullable final List<Task> taskList = serviceLocator.getTaskService()
                        .findTasksByProjectId(user.getId(), project.getId());

                System.out.println(++projectCount + ". " + project.toString());

                if (taskList != null) {
                    System.out.println("[TASK LIST]");

                    @Nullable final StringBuilder projectTasks = new StringBuilder();
                    int taskCount = 0;

                    for (@NotNull final Task task : taskList) {
                        projectTasks
                                .append(++taskCount)
                                .append(". ")
                                .append(task.toString())
                                .append(System.lineSeparator());
                    }

                    @NotNull final String taskString = projectTasks.toString();
                    System.out.println(taskString);
                } else {
                    System.out.println("[No tasks yet.]");
                }
            }
        }

        if (taskNotAssignedList != null) {
            System.out.println("[Not assigned TASKS LIST]");

            @NotNull final StringBuilder taskNotAssigned = new StringBuilder();
            int taskCount = 0;

            for (@NotNull final Task task : taskNotAssignedList) {
                taskNotAssigned
                        .append(++taskCount)
                        .append(". ")
                        .append(task.toString())
                        .append(System.lineSeparator());
            }

            @NotNull final String taskString = taskNotAssigned.toString();
            System.out.println(taskString);
        }

        if (projectList == null & taskNotAssignedList == null) {
            throw new Exception("[No projects and tasks yet.]");
        }
    }
}
