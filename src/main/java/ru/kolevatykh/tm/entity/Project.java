package ru.kolevatykh.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor(force = true)
@XmlAccessorType(XmlAccessType.PROPERTY)
public class Project extends AbstractProjectTaskEntity implements Serializable {

    public Project(@NotNull final String name, @NotNull final String description,
                   @Nullable final Date startDate, @Nullable final Date endDate) {
        super(name, description, startDate, endDate);
    }

    @Override
    public String toString() {
        return "user id: '" + userId + '\'' +
                ", id: '" + id + '\'' +
                ", name: '" + name + '\'' +
                ", status: '" + statusType + '\'' +
                ", description: '" + description + '\'' +
                ", startDate: " + startDate +
                ", endDate: " + endDate;
    }
}
