package ru.kolevatykh.tm.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.kolevatykh.tm.enumerate.RoleType;
import ru.kolevatykh.tm.util.PasswordHashUtil;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;

@Setter
@Getter
@NoArgsConstructor
@XmlAccessorType(XmlAccessType.PROPERTY)
public class User extends AbstractEntity implements Serializable {

    @NonNull
    private String login;

    @XmlTransient
    @NonNull
    private transient String passwordHash;

    @NonNull
    private RoleType roleType;

    private boolean auth = false;

    public User(@NotNull final String login,
                @NotNull final String password,
                @NotNull final RoleType roleType) {
        super();
        this.login = login;
        this.passwordHash = PasswordHashUtil.getPasswordHash(password);
        this.roleType = roleType;
    }

    @JsonIgnore
    @XmlTransient
    public void setPasswordHash(@NotNull final String password) {
        this.passwordHash = PasswordHashUtil.getPasswordHash(password);
    }

    @Override
    public String toString() {
        return "id: '" + id + '\'' +
                ", login: '" + login + '\'' +
                ", roleType: " + roleType +
                ", isAuth: " + auth;
    }
}
