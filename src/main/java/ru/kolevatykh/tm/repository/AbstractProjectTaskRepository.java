package ru.kolevatykh.tm.repository;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.entity.AbstractProjectTaskEntity;
import java.io.Serializable;
import java.util.*;

@Setter
@Getter
@NoArgsConstructor
public abstract class AbstractProjectTaskRepository<T extends AbstractProjectTaskEntity> implements Serializable {

    @NotNull
    protected final Map<String, T> map = new LinkedHashMap<>();

    @NotNull
    public final List<T> findAll(@NotNull final String userId) {
        @NotNull final List<T> list = new ArrayList<>();
        for (@NotNull final Map.Entry<String, T> entry : map.entrySet()) {
            if (userId.equals(entry.getValue().getUserId()))
                list.add(entry.getValue());
        }
        return list;
    }

    @Nullable
    public final T findOneById(@NotNull final String userId, @NotNull final String id) {
        for (@NotNull final Map.Entry<String, T> entry : map.entrySet()) {
            if (userId.equals(entry.getValue().getUserId())
                    && entry.getValue().getId().equals(id))
                return entry.getValue();
        }
        return null;
    }

    @Nullable
    public final T findOneByName(@NotNull final String userId, @NotNull final String name) {
        for (@NotNull final Map.Entry<String, T> entry : map.entrySet()) {
            if (userId.equals(entry.getValue().getUserId())
                    && name.equals(entry.getValue().getName()))
                return entry.getValue();
        }
        return null;
    }

    public void merge(@NotNull final T entity) {
        map.put(entity.getId(), entity);
    }

    public void remove(@NotNull final String userId, @NotNull final String id) {
        for (Iterator<Map.Entry<String, T>> it = map.entrySet().iterator(); it.hasNext(); ) {
            @NotNull final Map.Entry<String, T> entry = it.next();
            @NotNull final T entity = entry.getValue();
            if (userId.equals(entity.getUserId())
                    && entity.getId().equals(id))
                it.remove();
        }
    }

    public void removeAll(@NotNull final String userId) {
        for (Iterator<Map.Entry<String, T>> it = map.entrySet().iterator(); it.hasNext(); ) {
            Map.Entry<String, T> entry = it.next();
            if (userId.equals(entry.getValue().getUserId()))
                it.remove();
        }
    }

    @NotNull
    public final List<T> findAllSortedByStartDate(@NotNull final String userId) {
        @NotNull final List<T> list = new ArrayList<>();
        for (@NotNull final Map.Entry<String, T> entry : map.entrySet()) {
            if (userId.equals(entry.getValue().getUserId()))
                list.add(entry.getValue());
        }
        list.sort(new Comparator<T>() {
            @Override
            public int compare(T o1, T o2) {
                if (o1.getStartDate() == null || o2.getStartDate() == null)
                    return 0;
                return o1.getStartDate().compareTo(o2.getStartDate());
            }
        });
        return list;
    }

    @NotNull
    public final List<T> findAllSortedByEndDate(@NotNull final String userId) {
        @NotNull final List<T> list = new ArrayList<>();
        for (@NotNull final Map.Entry<String, T> entry : map.entrySet()) {
            if (userId.equals(entry.getValue().getUserId()))
                list.add(entry.getValue());
        }
        list.sort(new Comparator<T>() {
            @Override
            public int compare(T o1, T o2) {
                if (o1.getEndDate() == null || o2.getEndDate() == null)
                    return 0;
                return o1.getEndDate().compareTo(o2.getEndDate());
            }
        });
        return list;
    }

    @NotNull
    public final List<T> findAllSortedByStatus(@NotNull final String userId) {
        @NotNull final List<T> list = new ArrayList<>();
        for (@NotNull final Map.Entry<String, T> entry : map.entrySet()) {
            if (userId.equals(entry.getValue().getUserId()))
                list.add(entry.getValue());
        }
        list.sort(new Comparator<T>() {
            @Override
            public int compare(T o1, T o2) {
                return o1.getStatusType().compareTo(o2.getStatusType());
            }
        });
        return list;
    }

    @NotNull
    public final List<T> findAllBySearch(@NotNull final String userId, @NotNull final String search) {
        @NotNull final List<T> list = new ArrayList<>();
        for (@NotNull final Map.Entry<String, T> entry : map.entrySet()) {
            if (userId.equals(entry.getValue().getUserId())
                    & (entry.getValue().getName().contains(search)
                    | entry.getValue().getDescription().contains(search)))
                list.add(entry.getValue());
        }
        return list;
    }
}
