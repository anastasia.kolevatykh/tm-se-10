package ru.kolevatykh.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.entity.AbstractEntity;
import java.io.Serializable;
import java.util.*;

public abstract class AbstractRepository<T extends AbstractEntity> implements Serializable {

    @NotNull
    protected final Map<String, T> map = new LinkedHashMap<>();

    @NotNull
    public final List<T> findAll() {
        return new ArrayList<>(map.values());
    }

    @Nullable
    public final T findOneById(@NotNull final String id) {
        return map.get(id);
    }

    public void persist(@NotNull final T user) {
        map.put(user.getId(), user);
    }

    public void merge(@NotNull final T user) {
        map.put(user.getId(), user);
    }

    public void remove(@NotNull final String id) {
        for (Iterator<Map.Entry<String, T>> it = map.entrySet().iterator(); it.hasNext(); ) {
            @NotNull final Map.Entry<String, T> entry = it.next();
            @NotNull final T task = entry.getValue();
            if (task.getId().equals(id))
                it.remove();
        }
    }

    public void removeAll() {
        if (map.isEmpty()) return;
        map.clear();
    }
}
