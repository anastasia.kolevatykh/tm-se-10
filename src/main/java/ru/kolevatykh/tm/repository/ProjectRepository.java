package ru.kolevatykh.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.kolevatykh.tm.api.IProjectRepository;
import ru.kolevatykh.tm.entity.Project;

import java.io.*;

public final class ProjectRepository extends AbstractProjectTaskRepository<Project> implements IProjectRepository, Serializable {

    @Override
    public void persist(@NotNull final Project project) {
        map.put(project.getId(), project);
    }
}
