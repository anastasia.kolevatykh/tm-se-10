package ru.kolevatykh.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.api.IUserRepository;
import ru.kolevatykh.tm.entity.User;

import java.io.Serializable;
import java.util.*;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository, Serializable {

    @Nullable
    @Override
    public final User findOneByLogin(@NotNull final String login) {
        for (@NotNull final Map.Entry<String, User> entry : map.entrySet()) {
            if (entry.getValue().getLogin().equals(login))
                return entry.getValue();
        }
        return null;
    }
}
