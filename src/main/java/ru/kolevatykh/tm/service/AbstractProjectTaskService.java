package ru.kolevatykh.tm.service;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.entity.AbstractProjectTaskEntity;
import ru.kolevatykh.tm.api.IProjectTaskRepository;

import java.io.*;
import java.util.List;

@Setter
@Getter
@NoArgsConstructor
public abstract class AbstractProjectTaskService<T extends AbstractProjectTaskEntity> implements Serializable {

    @Nullable
    private IProjectTaskRepository<T> projectTaskRepository;

    AbstractProjectTaskService(@NotNull final IProjectTaskRepository<T> projectTaskRepository) {
        this.setProjectTaskRepository(projectTaskRepository);
    }

    @Nullable
    public final List<T> findAll(@Nullable final String userId) {
        if (projectTaskRepository == null || userId == null || userId.isEmpty()) return null;
        @NotNull final List<T> taskList = projectTaskRepository.findAll(userId);
        if (taskList.isEmpty()) return null;
        return taskList;
    }

    @Nullable
    public final T findOneById(@Nullable final String userId, @Nullable final String id) {
        if (projectTaskRepository == null
                || id == null || id.isEmpty()
                || userId == null || userId.isEmpty()) return null;
        return projectTaskRepository.findOneById(userId, id);
    }

    @Nullable
    public final T findOneByName(@Nullable final String userId, @Nullable final String name) {
        if (projectTaskRepository == null
                || name == null || name.isEmpty()
                || userId == null || userId.isEmpty()) return null;
        return projectTaskRepository.findOneByName(userId, name);
    }

    public void persist(@Nullable final T entity) {
        if (projectTaskRepository == null || entity == null) return;
        projectTaskRepository.persist(entity);
    }

    public void merge(@Nullable final T entity) {
        if (projectTaskRepository == null || entity == null) return;
        projectTaskRepository.merge(entity);
    }

    public void remove(@Nullable final String userId, @Nullable final String id) {
        if (projectTaskRepository == null
                || id == null || id.isEmpty()
                || userId == null || userId.isEmpty()) return;
        projectTaskRepository.remove(userId, id);
    }

    public void removeAll(@Nullable final String userId) {
        if (projectTaskRepository == null || userId == null || userId.isEmpty()) return;
        projectTaskRepository.removeAll(userId);
    }

    @Nullable
    public final List<T> findAllSortedByStartDate(@Nullable final String userId) {
        if (projectTaskRepository == null || userId == null || userId.isEmpty()) return null;
        @NotNull final List<T> taskList = projectTaskRepository.findAllSortedByStartDate(userId);
        if (taskList.isEmpty()) return null;
        return taskList;
    }

    @Nullable
    public final List<T> findAllSortedByEndDate(@Nullable final String userId) {
        if (projectTaskRepository == null || userId == null || userId.isEmpty()) return null;
        @NotNull final List<T> taskList = projectTaskRepository.findAllSortedByEndDate(userId);
        if (taskList.isEmpty()) return null;
        return taskList;
    }

    @Nullable
    public final List<T> findAllSortedByStatus(@Nullable final String userId) {
        if (projectTaskRepository == null || userId == null || userId.isEmpty()) return null;
        @NotNull final List<T> taskList = projectTaskRepository.findAllSortedByStatus(userId);
        if (taskList.isEmpty()) return null;
        return taskList;
    }

    @Nullable
    public final List<T> findAllBySearch(@Nullable final String userId, @Nullable final String search) {
        if (projectTaskRepository == null || userId == null || userId.isEmpty()
                || search == null || search.isEmpty()) return null;
        @NotNull final List<T> taskList = projectTaskRepository.findAllBySearch(userId, search);
        if (taskList.isEmpty()) return null;
        return taskList;
    }
}
