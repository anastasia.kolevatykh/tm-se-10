package ru.kolevatykh.tm.service;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.kolevatykh.tm.api.IProjectService;
import ru.kolevatykh.tm.entity.Project;
import ru.kolevatykh.tm.api.IProjectRepository;
import java.io.Serializable;

@Setter
@Getter
@NoArgsConstructor
public final class ProjectService extends AbstractProjectTaskService<Project> implements IProjectService, Serializable {

    @NotNull
    private IProjectRepository projectRepository;

    public ProjectService(@NotNull final IProjectRepository projectRepository) {
        super(projectRepository);
        this.setProjectRepository(projectRepository);
    }
}
