package ru.kolevatykh.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

public final class PasswordHashUtil {

    @NotNull
    public static String getPasswordHash(@NotNull final String password) {
        @Nullable MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        md.update(password.getBytes());
        byte[] digest = md.digest();
        @NotNull String passHash = Arrays.toString(digest);
        return passHash;
    }
}
